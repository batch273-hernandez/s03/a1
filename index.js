// In JS, classes can be created using the "class" keyword and {}
/*

//

Naming convention for classes: Begin with Uppercase characters

	Syntax:
		class NameOfClass {
	
		}

*/

// class Student {
	
// 	constructor(name, email) {
// 		// this.key = value/parameter
// 		this.name = name;
// 		this.email = email;
// 	}

// }

// let studentOne = new Student('john', 'john@mail.com');
// let studentTwo = new Student();

/*
	Mini-Activity:
		Create a new class called Person

		This person class should be able to instantiate a new object with ff fields:

		name,
		age (should be a number and must be greater than or equal to 18, otherwise set the property to undefined),
		nationality,
		address

		Instantiate 2 new objects from the Person class. person1 and person2

		Log both objects in the console.

*/

class Person {
	
	constructor(name, age, nationality, address) {
		
		this.name = name;
		if (typeof age === 'number' && age >= 18){
			this.age = age;
		} else {
			this.age = undefined;
		};		
		this.nationality = nationality;
		this.address = address;
	}
};

let person1 = new Person('Jane Doe', 17, 'American', 'New York');
let person2 = new Person('Kim Soo Hyun', 35, 'Korean', 'Seoul');
console.log(person1);
console.log(person2);


// ACTIVITY 1 - Quiz
/*			
	1. What is the blueprint where objects are created from?
		Ans. class
	2. What is the naming convention applied to classes?
		Ans. begin with uppercase characters
	3. What keyword do we use to create objects from a class?
		Ans. new
	4. What is the technical term for creating an object from a class?
		Ans. instantiate
	5. What class method dictates HOW objects will be created from that class?
		Ans. constructor
*/


// ACTIVITY 1 - Function Coding

class Student {
  	constructor(name, email, grades) {
	    this.name = name;
	    this.email = email;
	    this.gradeAve = undefined;
	    this.passed = undefined;
	    this.passedWithHonors = undefined;

	    if(grades.length === 4) {
	    	if(grades.every(grade => grade >= 0 && grade <= 100)){
	    		this.grades = grades;
	    	} else {
	    		this.grades = undefined
	    	}
	    } else {
	    	this.grades = undefined
	    }

    // My Solution
    // Array.isArray() is a built-in method in JavaScript that checks whether a given value is an array or not. It takes one parameter and returns a boolean value of true if the parameter is an array, and false otherwise.
    // if (Array.isArray(grades) && grades.length === 4 &&
    //     grades.every(grade => typeof grade === 'number' && grade >= 0 && grade <= 100)) {
    //   this.grades = grades;
    // } else {
    //   this.grades = undefined;
    // }
  	}

	// Methods
	login() {
		console.log(`${this.email} has logged in`);
		console.log(this)
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this;
	}
	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}.`);
		return this;
	}

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		// update property
		this.gradeAve = sum/this.grades.length
		// return object
		return this;
	}

	// willPass(){
	// 	return this.computeAve() >= 85 ? true : false	    
	// }

	willPass(){
		this.passed = this.computeAve().gradeAve >= 85 ? true : false
		return this;    
	}	

	willPassWithHonors() {
		if(this.passed) {
			if(this.gradeAve >= 90){
				this.passedWithHonors = true;
			} else {
				this.passedWithHonors = false;
			}
		} else {
			this.passedWithHonors = false;
		}
		return this
	}
}

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);

// Getter and Setter
// Best practice dictates that we regulate access to such properties
// Getter - retrieval
// Setter - manipulation

// ACTIVITY 2 - Quiz
/*
	1. Should class methods be included in the class constructor?
		Ans. No
	2. Can class methods be separated by commas?
		Ans. No
	3. Can we update an object's properties via dot notation?
		Ans. Yes
	4. What do you call the methods used to regulate access to an object's properties?
		Ans. Setter and Getter
	5. What does a method need to return in order for it to be chainable?
		Ans. this
*/